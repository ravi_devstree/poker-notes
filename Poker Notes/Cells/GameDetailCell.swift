//
//  GameDetailCell.swift
//  Poker Notes
//
//  Created by Ravi on 17/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class GameDetailCell: UITableViewCell {

    @IBOutlet weak var gameIcon: UIImageView!
    @IBOutlet weak var lblGameType: UILabel!
    @IBOutlet weak var lblTotalPlayers: UILabel!
    @IBOutlet weak var lblBuyIn: UILabel!
    @IBOutlet weak var lblBlinds: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var stackBlinds: UIStackView!
    @IBOutlet weak var stackTotalPlayer: UIStackView!
    @IBOutlet weak var stackBuyIn: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(game: RealmGame) {
        gameIcon.image = game.gameType.icon
        lblGameType.text = game.gameType == .cash ? "Cash Game" : "Tournament"
        lblTotalPlayers.text = "\(game.totalPlayers)"
        lblBlinds.text = "\(game.smallBlinds)/\(game.bigBlinds)"
        lblLocation.text = game.location
        lblName.text = game.name
        lblBuyIn.text = "\(game.buyIn)"
        
        switch game.gameType {
        case .cash:
            stackBlinds.isHidden = false
            stackBuyIn.isHidden = true
            stackTotalPlayer.isHidden = true
        case .tournament:
            stackBlinds.isHidden = true
            stackBuyIn.isHidden = false
            stackTotalPlayer.isHidden = false
        }
    }
    
}
