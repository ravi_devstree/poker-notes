//
//  GameCell.swift
//  Poker Notes
//
//  Created by Ravi on 16/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class GameCell: UITableViewCell {

    @IBOutlet weak var lblHands: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var gameIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(_ game: RealmGame) {
        lblName.text = game.name
        lblLocation.text = game.location
        lblDate.text = game.date.toString()
        lblHands.text = "\(game.handsCount) " + ([0, 1].contains(game.handsCount) ? "Hand" : "Hands")
        
        gameIcon.image = game.gameType.icon
    }

}
