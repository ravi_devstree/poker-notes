//
//  GameHandCell.swift
//  Poker Notes
//
//  Created by Ravi Goswami on 26/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class GameHandCell: UITableViewCell {
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblPlayerCard: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(_ gameHand: RealmHand) {
        lblDate.text = gameHand.date.toString(formate: "hh:mm a, MM/dd/yyyy")
        guard let heroInfo = Array(gameHand.playerInfo).first(where: {$0.name.lowercased().contains("hero")}) else {return}
        lblPlayerCard.text = heroInfo.card1 + heroInfo.card2
    }
    
}
