//
//  UserCardView.swift
//  Poker Notes
//
//  Created by Ravi on 18/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class UserCardView: NibLoadingView, EditingView {
    func setEnabled(_ status: Bool) {
        txtPlayerName.isEnabled = status
        txtHands.isEnabled = status
        txtPosition.isEnabled = status
        txtStack.isEnabled = status
    }
    
    var playerInfo: RealmPlayer
    
    var card1 = ""
    var card2 = ""
    
    @IBOutlet weak var txtPlayerName: UITextField! {
        didSet {
            txtPlayerName.delegate = self
        }
    }
    @IBOutlet weak var txtHands: UITextField! {
        didSet {
            txtHands.delegate = self
        }
    }
    @IBOutlet weak var txtPosition: UITextField! {
        didSet {
            txtPosition.delegate = self
        }
    }
    @IBOutlet weak var txtStack: UITextField! {
        didSet {
            txtStack.delegate = self
        }
    }
    
    var cardPickerView = UIPickerView()
    var positionPickerView = UIPickerView()
    var cardUpdate: (() -> Void)?
    
    init(player: RealmPlayer, cardUpdate: (() -> Void)? = nil) {
        self.cardUpdate = cardUpdate
        self.playerInfo = player
        super.init(frame: .zero)
        txtPlayerName.text = playerInfo.name
        txtHands.text = player.card1 + " " + player.card2
        txtPosition.text = player.position
        txtStack.text = player.stackSize == 0 ? "" : "\(player.stackSize)"
        
        card1 = player.card1
        card2 = player.card2
        
        cardPickerView.delegate = self
        cardPickerView.dataSource = self
        
        positionPickerView.delegate = self
        positionPickerView.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func gerUpdatePlayerInfo() -> RealmPlayer {
        playerInfo.name = txtPlayerName.text ?? ""
        playerInfo.card1 = card1
        playerInfo.card2 = card2
        playerInfo.position = txtPosition.text ?? ""
        playerInfo.stackSize = Int(txtStack.text ?? "") ?? 0
        return playerInfo
    }
    
}

extension UserCardView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtPosition {
            textField.text = positionList[cardPickerView.selectedRow(inComponent: 0)]
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtHands {
            textField.inputView = cardPickerView
            card1 = ""
            card2 = ""
        }
        if textField == txtPosition {
            textField.inputView = positionPickerView
        }
    }
}

extension UserCardView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == cardPickerView {
            return 4
        } else {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == positionPickerView {
            return positionList.count
        }
        switch component {
        case 0,2:
            return 13
        case 1,3:
            return 4
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == positionPickerView {
            return positionList[row]
        }
        switch component {
        case 0,2:
            return cardRankList[row]
        case 1,3:
            return cardSuiteList[row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == positionPickerView {
            let position = positionList[row]
            txtPosition.text = position
            return
        }
        
        self.card1 = ""
        self.card2 = ""
        txtHands.text = ""
        
        cardUpdate?()
        
        let card1 = cardRankList[pickerView.selectedRow(inComponent: 0)] + cardSuiteList[pickerView.selectedRow(inComponent: 1)]
        let card2 = cardRankList[pickerView.selectedRow(inComponent: 2)] + cardSuiteList[pickerView.selectedRow(inComponent: 3)]
        
        if selectedCards.contains(card1) || selectedCards.contains(card2) {
            self.card1 = ""
            self.card2 = ""
            txtHands.text = ""
            return
        }
        
        if card1 == card2 {
            txtHands.text = ""
            self.card1 = ""
            self.card2 = ""
        } else {
            txtHands.text = card1 + " " + card2
            self.card1 = card1
            self.card2 = card2
            selectedCards.append(card1)
            selectedCards.append(card2)
        }
    }
}
