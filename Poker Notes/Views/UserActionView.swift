//
//  UserActionView.swift
//  Poker Notes
//
//  Created by Ravi on 18/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class UserActionView: NibLoadingView, EditingView {
    func setEnabled(_ status: Bool) {
        txtAction.isEnabled = status
    }
    
    var handAction: RealmHandAction
    
    init(_ handAction: RealmHandAction) {
        self.handAction = handAction
        super.init(frame: .zero)
        lblActionCount.text = "#\(self.handAction.order)"
        txtAction.text = handAction.actionString
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var lblActionCount: UILabel!
    @IBOutlet weak var txtAction: UITextField! {
        didSet {
            txtAction.delegate = self
        }
    }
    
    func getUpdatedHandAction() -> RealmHandAction {
        handAction.actionString = txtAction.text ?? ""
        return handAction
    }
}

extension UserActionView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
