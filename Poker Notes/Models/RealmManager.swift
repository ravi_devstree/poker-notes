//
//  RealmManager.swift
//  Immerch New
//
//  Created by Ravi on 26/09/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import RealmSwift
import Foundation

class RealmManager: NSObject {
    fileprivate var realm: Realm!
    static let shared: RealmManager = {
        let manager = RealmManager()
        do {
            try manager.realm = Realm()
        } catch let error {
            DPrint(error)
        }
        return manager
    }()
    
    fileprivate let semaphore = DispatchSemaphore(value: 0)
    
    func write(withoutNotifying shouldNotify: Bool = true, _ block: @escaping () -> Void) {
        DispatchQueue.main.async {
            do {
                if shouldNotify {
                    try self.realm.write(block)
                } else {
//                    try self.realm.write(withoutNotifying: [RealmMessageManager.shared.activeChatNotificationToken!], block)
                }
                self.semaphore.signal()
            } catch let error {
                DPrint(error)
                self.semaphore.signal()
            }
            self.semaphore.wait()
        }
    }
}
