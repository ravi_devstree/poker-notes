//
//  RealmHandAction.swift
//  Poker Notes
//
//  Created by Ravi on 17/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import Foundation
import RealmSwift

enum GameState: String {
    case preflop
    case postflop
    case turn
    case river
    case none
}

class RealmHandAction: Object {
    @objc dynamic var order: Int = 0
    @objc dynamic var actionString: String = ""
    @objc dynamic var gameStateStr: String = ""
}

extension RealmHandAction {
    var gameState: GameState {
        set {
            self.gameStateStr = newValue.rawValue
        }
        get {
            return GameState(rawValue: gameStateStr) ?? .none
        }
    }
}
