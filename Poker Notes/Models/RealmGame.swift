//
//  RealmGame.swift
//  Poker Notes
//
//  Created by Ravi on 16/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import Foundation
import RealmSwift

class RealmGame: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var location: String = ""
    @objc dynamic var smallBlinds: Int = 0
    @objc dynamic var bigBlinds: Int = 0
    @objc dynamic var buyIn: Int = 0
    @objc dynamic var date: Date = Date()
    @objc dynamic var totalPlayers: Int = 0
    @objc dynamic var handsCount: Int = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension RealmGame {
    var gameType: GameType {
        if totalPlayers == 0 {
            return .cash
        } else {
            return .tournament
        }
    }
}
