//
//  RealmGameManager.swift
//  Poker Notes
//
//  Created by Ravi on 16/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import RealmSwift
import Foundation

class RealmGameManager: NSObject {
    fileprivate var realm: Realm!
    static let shared: RealmGameManager = {
        let manager = RealmGameManager()
        do {
            manager.realm = try Realm()
        } catch {
            DPrint(error)
        }
        return manager
    }()
    
    func newCashGame(name: String, location: String, smallBlinds: Int, bigBlinds: Int, date: Date) -> RealmGame {
        let newGame = RealmGame()
        newGame.id = Int(Date().timeIntervalSince1970)
        newGame.name = name
        newGame.location = location
        newGame.smallBlinds = smallBlinds
        newGame.bigBlinds = bigBlinds
        newGame.date = date
        newGame.name = name
        newGame.buyIn = 0
        newGame.totalPlayers = 0
        
        RealmManager.shared.write {
            self.realm.add(newGame)
        }
        
        return newGame
    }
    
    func newTournament(name: String, location: String, buyIn: Int, totalPlayers: Int, date: Date) -> RealmGame {
        let newGame = RealmGame()
        newGame.id = Int(Date().timeIntervalSince1970)
        newGame.name = name
        newGame.location = location
        newGame.date = date
        newGame.name = name
        newGame.buyIn = buyIn
        newGame.totalPlayers = totalPlayers
        
        RealmManager.shared.write {
            self.realm.add(newGame)
        }
        
        return newGame
    }
    
    func delete(hand: RealmHand) {
        RealmManager.shared.write {
            hand.game.handsCount -= 1
            self.realm.delete(hand)
        }
    }
    
    func delete(game: RealmGame) {
        RealmManager.shared.write {
            self.realm.delete(game)
        }
    }
    
    func saveGameHand(_ gameHand: RealmHand) {
        RealmManager.shared.write {
            if self.realm.object(ofType: RealmHand.self, forPrimaryKey: gameHand.id) == nil {            
                gameHand.game.handsCount += 1
            }
            self.realm.add(gameHand, update: Realm.UpdatePolicy.modified)
        }
    }
    
    fileprivate weak var gameHandNotificationToken: NotificationToken? = nil
    func getGameHand(_ game: RealmGame, initialList: @escaping (([RealmHand]) -> Void), updates: @escaping ((_ list: [RealmHand], _ deletions: [Int], _ insertions: [Int],_ modifications: [Int]) -> Void)) {
        let rlm = try! Realm()
        let result = rlm.objects(RealmHand.self).filter("game == %@", game).sorted(byKeyPath: "date")
//        initialList(Array(result))
        gameHandNotificationToken = result.observe({ (changes) in
            switch changes {
            case .initial(let list):
                initialList(Array(list))
            case .update(let list, let deletions , let insertions, let modifications):
                updates(Array(list), deletions, insertions, modifications)
            case .error(let error):
                DPrint(error)
            }
        })
    }
    
    fileprivate var gameNotificationToken: NotificationToken? = nil
    func fetchAndObserverGame(initialList: @escaping (([RealmGame]) -> Void), updates: @escaping ((_ list: [RealmGame], _ deletions: [Int], _ insertions: [Int],_ modifications: [Int]) -> Void)) {
        let result = realm.objects(RealmGame.self)
        gameNotificationToken = result.observe({ (changes) in
            switch changes {
            case .initial(let list):
                initialList(Array(list))
            case .update(let list, let deletions , let insertions, let modifications):
                updates(Array(list), deletions, insertions, modifications)
            case .error(let error):
                DPrint(error)
            }
        })
    }
}
