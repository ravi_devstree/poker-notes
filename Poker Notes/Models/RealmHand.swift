//
//  RealmHand.swift
//  Poker Notes
//
//  Created by Ravi on 17/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import Foundation
import RealmSwift

class RealmHand: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var date: Date = Date()
    @objc dynamic var preFlopNote = ""
    @objc dynamic var postFlopNote = ""
    @objc dynamic var turnNote = ""
    @objc dynamic var riverNote = ""
    @objc dynamic var flopCards = ""
    @objc dynamic var turnCards = ""
    @objc dynamic var riverCards = ""
    @objc dynamic var level = ""
    @objc dynamic var playersLeft = ""
    @objc dynamic var smallBlinds = ""
    @objc dynamic var bigBlinds = ""
    dynamic var playerInfo = List<RealmPlayer>()
    dynamic var actions = List<RealmHandAction>()
    @objc var game: RealmGame!
    
    override class func primaryKey() -> String? {
        return "id"
    }
}


class RealmPlayer: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var card1: String = ""
    @objc dynamic var card2: String = ""
    @objc dynamic var position: String = ""
    @objc dynamic var stackSize: Int = 0
}
