//
//  NewGameHandVC.swift
//  Poker Notes
//
//  Created by Ravi on 17/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

protocol EditingView {
    func setEnabled(_ status: Bool)
}

class NewGameHandVC: UIViewController {
    
    var game: RealmGame
    
    init(game: RealmGame) {
        self.game = game
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var playerInfoStack: UIStackView!
    @IBOutlet weak var preflopActionStackView: UIStackView!
    @IBOutlet weak var postflopActionStackView: UIStackView!
    @IBOutlet weak var turnActionStackView: UIStackView!
    @IBOutlet weak var riverActionStackView: UIStackView!
    
    @IBOutlet var actionStackCollection: [UIStackView]!
    @IBOutlet var newActionButtonCollection: [UIButton]!
    
    @IBOutlet weak var txtPreflopNote: UITextView!
    @IBOutlet weak var txtPostflopNote: UITextView!
    @IBOutlet weak var txtTurnNote: UITextView!
    @IBOutlet weak var txtRiverNote: UITextView!
    
    @IBOutlet var noteTextViewCollection: [UITextView]!
    @IBOutlet var placeHolderLabelCollection: [UILabel]!
    @IBOutlet weak var btnAddNewOpponent: UIButton!
    
    @IBOutlet weak var txtSmallBlind: UITextField?
    @IBOutlet weak var txtBigBlind: UITextField?
    @IBOutlet weak var txtLevel: UITextField?
    @IBOutlet weak var txtPlayersLeft: UITextField?
    
    @IBOutlet weak var cashGameView: UIView!
    @IBOutlet weak var lblFlopCards: UILabel! {
        didSet {
            lblFlopCards.text = ""
        }
    }
    @IBOutlet weak var lblTurnCard: UILabel! {
        didSet {
            lblTurnCard.text = ""
        }
    }
    @IBOutlet weak var lblRiverCard: UILabel! {
        didSet {
            lblRiverCard.text = ""
        }
    }
    
    @IBOutlet weak var txtFlopCards: UITextField! {
        didSet {
            txtFlopCards.delegate = self
        }
    }
    @IBOutlet weak var txtTurnCards: UITextField! {
        didSet {
            txtTurnCards.delegate = self
        }
    }
    @IBOutlet weak var txtRiverCards: UITextField! {
        didSet {
            txtRiverCards.delegate = self
        }
    }
    
    var gameHand: RealmHand?
    var cardPicker = UIPickerView()
    var onNewHandSave: ((RealmHand) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Hand"
        
        cardPicker.delegate = self
        cardPicker.dataSource = self
        
        selectedCards.removeAll()
        
        noteTextViewCollection.forEach { (tv) in
            tv.delegate = self
            tv.tag = noteTextViewCollection.firstIndex(of: tv) ?? 999
        }
        
        placeHolderLabelCollection.forEach { (lbl) in
            lbl.tag = placeHolderLabelCollection.firstIndex(of: lbl) ?? 999
        }
        
        actionStackCollection.forEach { (stack) in
            stack.tag = actionStackCollection.firstIndex(of: stack) ?? 999
        }
        
        if game.gameType == .cash {
            cashGameView.isHidden = true
            cashGameView.removeFromSuperview()
        }
        
        let saveBarButton = UIBarButtonItem(title: gameHand == nil ? "Save" : "Update", style: .done, target: self, action: #selector(saveAction(_:)))
        self.navigationItem.rightBarButtonItem = saveBarButton
        
        if let hand = gameHand {
            self.setupHandDetails(gameHand: hand)
            newActionButtonCollection.forEach { (btn) in
                btn.isHidden = true
            }
        } else {
            let hero = RealmPlayer()
            hero.name = "Hero"
            playerInfoStack.addArrangedSubview(UserCardView(player: hero, cardUpdate: updateSelectedCard))
            
            let opponent1 = RealmPlayer()
            opponent1.name = "Villain 1"
            playerInfoStack.addArrangedSubview(UserCardView(player: opponent1, cardUpdate: updateSelectedCard))
            
            newActionButtonCollection.forEach { (btn) in
                btn.addTarget(self, action: #selector(addNewAction(_:)), for: UIControl.Event.touchUpInside)
                btn.tag = (newActionButtonCollection.firstIndex(of: btn) ?? 999)
                for _ in 0...4 {                
                    self.addNewAction(btn)
                }
            }
        }
    }
    
    func setupHandDetails(gameHand: RealmHand) {
//        btnAddNewOpponent.isHidden = true
        
        lblFlopCards.text = gameHand.flopCards
        txtFlopCards.text = gameHand.flopCards
        
        lblTurnCard.text = gameHand.turnCards
        txtTurnCards.text = gameHand.turnCards
        
        lblRiverCard.text = gameHand.riverCards
        txtRiverCards.text = gameHand.riverCards
        
        txtLevel?.text = gameHand.level
        txtPlayersLeft?.text = gameHand.playersLeft
        txtSmallBlind?.text = gameHand.smallBlinds
        txtBigBlind?.text = gameHand.bigBlinds
        
//        noteTextViewCollection.forEach { (tv) in
//            tv.isEditable = false
//        }
        
        gameHand.playerInfo.forEach({ (playerInfo) in
            let userInfoView = UserCardView(player: playerInfo, cardUpdate: updateSelectedCard)
//            userInfoView.setEnabled(false)
            playerInfoStack.addArrangedSubview(userInfoView)
        })
        
        let preFlopActions = Array(gameHand.actions).filter({$0.gameState == .preflop})
        preFlopActions.map({UserActionView($0)}).forEach({
//            $0.setEnabled(false)
            preflopActionStackView.addArrangedSubview($0)
        })
        txtPreflopNote.text = gameHand.preFlopNote
        
        let postFlopActions = Array(gameHand.actions).filter({$0.gameState == .postflop})
        postFlopActions.map({UserActionView($0)}).forEach({
//            $0.setEnabled(false)
            postflopActionStackView.addArrangedSubview($0)
        })
        txtPostflopNote.text = gameHand.postFlopNote
        
        let turnActions = Array(gameHand.actions).filter({$0.gameState == .turn})
        turnActions.map({UserActionView($0)}).forEach({
//            $0.setEnabled(false)
            turnActionStackView.addArrangedSubview($0)
        })
        txtTurnNote.text = gameHand.turnNote
        
        let riverActions = Array(gameHand.actions).filter({$0.gameState == .river})
        riverActions.map({UserActionView($0)}).forEach({
//            $0.setEnabled(false)
            riverActionStackView.addArrangedSubview($0)
        })
        txtRiverNote.text = gameHand.riverNote
        
        noteTextViewCollection.forEach({textViewDidChange($0)})
    }
    
    @IBAction func addNewPlayerInfo(_ sender: UIButton) {
        let newOpponent = RealmPlayer()
        newOpponent.name = "Villain \(playerInfoStack.arrangedSubviews.count)"
        self.playerInfoStack.addArrangedSubview(UserCardView(player: newOpponent, cardUpdate: updateSelectedCard))
    }
    
    @objc func addNewAction(_ sender: UIButton) {
        guard let actionStack = actionStackCollection.first(where: {$0.tag == sender.tag}) else {return}
        let handAction = RealmHandAction()
        switch sender.tag {
        case 0:
            handAction.gameState = .preflop
        case 1:
            handAction.gameState = .postflop
        case 2:
            handAction.gameState = .turn
        case 3:
            handAction.gameState = .river
        default:
            handAction.gameState = .none
        }
        handAction.order = actionStack.arrangedSubviews.count + 1
        actionStack.addArrangedSubview(UserActionView(handAction))
    }
    
    @objc func saveAction(_ sender: UIBarButtonItem) {
        let gameHand = self.gameHand ?? RealmHand()
        if self.gameHand == nil {
            gameHand.id = Int(Date().timeIntervalSince1970)
            gameHand.date = Date()
        }
        RealmManager.shared.write {
            let playerInfo = self.playerInfoStack.arrangedSubviews.compactMap({($0 as? UserCardView)?.gerUpdatePlayerInfo()})
            gameHand.playerInfo.removeAll()
            gameHand.playerInfo.append(objectsIn: playerInfo)
            
            var actionList = [RealmHandAction]()
            
            let preFlopActions = self.preflopActionStackView.arrangedSubviews.compactMap({($0 as? UserActionView)?.getUpdatedHandAction()})
            actionList.append(contentsOf: preFlopActions)
            gameHand.preFlopNote = self.txtPreflopNote.text
            
            let postFlopActions = self.postflopActionStackView.arrangedSubviews.compactMap({($0 as? UserActionView)?.getUpdatedHandAction()})
            actionList.append(contentsOf: postFlopActions)
            gameHand.postFlopNote = self.txtPostflopNote.text
            
            let turnFlopActions = self.turnActionStackView.arrangedSubviews.compactMap({($0 as? UserActionView)?.getUpdatedHandAction()})
            actionList.append(contentsOf: turnFlopActions)
            gameHand.turnNote = self.txtTurnNote.text
            
            let riverFlopActions = self.riverActionStackView.arrangedSubviews.compactMap({($0 as? UserActionView)?.getUpdatedHandAction()})
            actionList.append(contentsOf: riverFlopActions)
            gameHand.riverNote = self.txtRiverNote.text
            
            gameHand.actions.removeAll()
            gameHand.actions.append(objectsIn: actionList)
            
            gameHand.level = self.txtLevel?.text ?? ""
            gameHand.playersLeft = self.txtPlayersLeft?.text ?? ""
            gameHand.smallBlinds = self.txtSmallBlind?.text ?? ""
            gameHand.bigBlinds = self.txtBigBlind?.text ?? ""
            
            gameHand.flopCards = self.txtFlopCards.text ?? ""
            gameHand.turnCards = self.txtTurnCards.text ?? ""
            gameHand.riverCards = self.txtRiverCards.text ?? ""
            
            gameHand.game = self.game
            
            RealmGameManager.shared.saveGameHand(gameHand)
            
            if self.gameHand == nil {
                self.onNewHandSave?(gameHand)
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func updateSelectedCard() {
        selectedCards.removeAll()
        self.playerInfoStack.arrangedSubviews.compactMap({$0 as? UserCardView}).forEach({
            selectedCards.append($0.card1)
            selectedCards.append($0.card2)
        })
        
        selectedCards.append(contentsOf: (txtFlopCards.text ?? "").components(separatedBy: " "))
        selectedCards.append(txtTurnCards.text ?? "")
        selectedCards.append(txtRiverCards.text ?? "")
    }
}

extension NewGameHandVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.inputView = cardPicker
        cardPicker.reloadAllComponents()
    }
}

extension NewGameHandVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        placeHolderLabelCollection.first(where: {$0.tag == textView.tag})?.isHidden = !textView.text.isEmpty
    }
}

extension NewGameHandVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if txtFlopCards.isEditing {
            return 6
        }
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0, 2, 4:
            return 13
        case 1, 3, 5:
            return 4
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0, 2, 4:
            return cardRankList[row]
        case 1, 3, 5:
            return cardSuiteList[row]
        default:
            return nil
        }
    }
    
    func selectedString(inComponent component: Int) -> String {
        if component % 2 == 0 {
            return cardRankList[cardPicker.selectedRow(inComponent: component)]
        } else {
            return cardSuiteList[cardPicker.selectedRow(inComponent: component)]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtFlopCards.isEditing {
            txtFlopCards.text = ""
            updateSelectedCard()
            let card1 = selectedString(inComponent: 0) + selectedString(inComponent: 1)
            let card2 = selectedString(inComponent: 2) + selectedString(inComponent: 3)
            let card3 = selectedString(inComponent: 4) + selectedString(inComponent: 5)
            
            if selectedCards.contains(card1) || selectedCards.contains(card2) || selectedCards.contains(card3) {
                RealmManager.shared.write {
                    self.gameHand?.flopCards = ""
                }
                txtFlopCards.text = ""
                lblFlopCards.text = ""
                return
            }
            
            var card = ""
            if card1 != card2 && card2 != card3 && card1 != card3 {
                card = card1 + " " + card2 + " " + card3
                RealmManager.shared.write {
                    self.gameHand?.flopCards = card
                }
            } else {
                RealmManager.shared.write {
                    self.gameHand?.flopCards = ""
                }
            }
            
            txtFlopCards.text = card
            lblFlopCards.text = card
        }
        
        if txtTurnCards.isEditing {
            txtTurnCards.text = ""
            updateSelectedCard()
            let card = selectedString(inComponent: 0) + selectedString(inComponent: 1)
            
            if selectedCards.contains(card) {
                txtTurnCards.text = ""
                RealmManager.shared.write {
                    self.gameHand?.turnCards = ""
                }
                lblTurnCard.text = ""
                return
            }
            
            txtTurnCards.text = card
            RealmManager.shared.write {
                self.gameHand?.turnCards = card
            }
            lblTurnCard.text = card
        }
        
        if txtRiverCards.isEditing {
            txtRiverCards.text = ""
            updateSelectedCard()
            let card = selectedString(inComponent: 0) + selectedString(inComponent: 1)
            
            if selectedCards.contains(card) {
                txtRiverCards.text = ""
                RealmManager.shared.write {
                    self.gameHand?.riverCards = ""
                }
                lblRiverCard.text = ""
                return
            }
            
            txtRiverCards.text = card
            RealmManager.shared.write {
                self.gameHand?.riverCards = card
            }
            lblRiverCard.text = card
        }
        
        updateSelectedCard()
    }
}
