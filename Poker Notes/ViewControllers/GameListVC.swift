//
//  GameListVC.swift
//  Poker Notes
//
//  Created by Ravi on 16/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import MessageUI
import SwiftyStoreKit

class GameListVC: UITableViewController {
    
    var gameList = [RealmGame]()
    var filteredList = [RealmGame]()

    @IBOutlet weak var filterSegment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        tableView.tableFooterView = UIView()
        fetchGameList()
    }
    
    func subscriptionCheck() {
        if SubscriptionExpiryDate < Date() {
            AppNotification.showInfoMessage("Please verify your subscription")
            UserDefaults.standard.set(false, forKey: monthSubscriptionId)
            UserDefaults.standard.set(false, forKey: yearSubscriptionId)
            openSubscriptionScreen()
        } else if UserDefaults.standard.bool(forKey: monthSubscriptionId) {
            verifySubscription(forId: monthSubscriptionId)
        } else if UserDefaults.standard.bool(forKey: yearSubscriptionId) {
            verifySubscription(forId: yearSubscriptionId)
        } else {
            openSubscriptionScreen()
        }
    }
    
    func openSubscriptionScreen() {
        let subscriptionVC = SubscriptionViewController()
        subscriptionVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(subscriptionVC, animated: true, completion: nil)
    }
    
    func verifySubscription(forId productId: String) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            if case .success(let receipt) = result {
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable,
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let expiryDate, let receiptItems):
//                    AppNotification.showInfoMessage("Product is valid until \(expiryDate.toString())")
                    SubscriptionExpiryDate = expiryDate
                case .expired(let expiryDate, let receiptItems):
                    AppNotification.showWarningMessage("Product is expired since \(expiryDate.toString())")
                    self.openSubscriptionScreen()
                case .notPurchased:
                    print("This product has never been purchased")
                }
            } else if case .error(let error) = result {
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchGameList() {
        RealmGameManager.shared.fetchAndObserverGame(initialList: { [weak self] (list) in
            guard let self = self else {return}
            self.gameList = list
            self.filterGameList(self.filterSegment)
        }) {[weak self] (list, deletions, insertions, modifications) in
            guard let self = self else {return}
            self.gameList = list
            self.filterGameList(self.filterSegment)
        }
    }
    
    @IBAction func filterGameList(_ sender: UISegmentedControl) {
        switch filterSegment.selectedSegmentIndex {
        case 0:
            filteredList = gameList.filter({$0.gameType == .cash})
        case 1:
            filteredList = gameList.filter({$0.gameType == .tournament})
        default:
            break
        }
        self.tableView.reloadSections([0], with: .automatic)
    }
    
    func newGame(forType type: GameType) {
        if gameList.count >= 10 {
            subscriptionCheck()
            return
        }
        
        let newGameVC = NewGameVC(gameType: type)
        newGameVC.onNewGame = { [weak self] newGame in
            self?.showGameDetails(newGame)
        }
        let navC = UINavigationController(rootViewController: newGameVC)
        self.present(navC, animated: true, completion: nil)
    }
    
    @IBAction func sendFeedbackMail(_ sender: UIBarButtonItem) {
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([feedbackEmail])
        mailComposeVC.setSubject("Hold'em Stack Feedback")
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeVC, animated: true, completion: nil)
        } else {
            AppNotification.showErrorMessage("Please configure email!")
        }
    }
    
    @IBAction func newGameAction(_ sender: UIBarButtonItem) {
        self.newGame(forType: filterSegment.selectedSegmentIndex == 0 ? .cash : .tournament)
    }
    
    fileprivate func showGameDetails(_ game: RealmGame) {
        self.navigationController?.pushViewController(GameDetailsVC(game: game), animated: true)
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath) as! GameCell
        cell.loadCell(filteredList[indexPath.row])
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showGameDetails(filteredList[indexPath.row])
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            RealmGameManager.shared.delete(game: filteredList[indexPath.row])
//            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GameListVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let error = error {
            AppNotification.showWarningMessage(error.localizedDescription)
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
