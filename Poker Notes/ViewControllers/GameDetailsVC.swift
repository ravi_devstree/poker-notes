//
//  GameDetailsVC.swift
//  Poker Notes
//
//  Created by Ravi on 17/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit
import RealmSwift

class GameDetailsVC: UITableViewController {
    
    var game: RealmGame
    
    init(game: RealmGame) {
        self.game = game
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var handList = [RealmHand]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = game.name
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "GameDetailCell", bundle: nil), forCellReuseIdentifier: "GameDetailCell")
        self.tableView.register(UINib(nibName: "GameHandCell", bundle: nil), forCellReuseIdentifier: "GameHandCell")

        let newHandButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(startNewHand(_:)))
        self.navigationItem.rightBarButtonItem = newHandButton
        
        fetchGameHand()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadSections([1], with: .automatic)
    }
    
    fileprivate weak var gameHandNotificationToken: NotificationToken? = nil
    func fetchGameHand() {
        let realm = try! Realm()
        let result = realm.objects(RealmHand.self).filter("game == %@", game).sorted(byKeyPath: "date")
        self.handList = Array(result)
        self.tableView.reloadData()
    }
    
    @objc func startNewHand(_ sender: UIBarButtonItem) {
        let newGameHandVC = NewGameHandVC(game: game)
        newGameHandVC.onNewHandSave = { [weak self] gameHand in
            guard let self = self else {return}
            self.handList.append(gameHand)
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(newGameHandVC, animated: true)
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return handList.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GameDetailCell", for: indexPath) as! GameDetailCell
            cell.loadCell(game: game)
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GameHandCell", for: indexPath) as! GameHandCell
            cell.lblNumber.text = "#\(indexPath.row + 1)"
            cell.loadCell(handList[indexPath.row])
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            let handDetailVC = NewGameHandVC(game: game)
            handDetailVC.gameHand = handList[indexPath.row]
            self.navigationController?.pushViewController(handDetailVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return nil
        }
        return "Hands"
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 32
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return indexPath.section != 0
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            RealmGameManager.shared.delete(hand: handList[indexPath.row])
            self.handList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
