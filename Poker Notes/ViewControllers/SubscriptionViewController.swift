//
//  SubscriptionViewController.swift
//  Poker Notes
//
//  Created by Ravi Goswami on 27/01/20.
//  Copyright © 2020 DevsTree. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyStoreKit

class SubscriptionViewController: UIViewController {

    @IBOutlet weak var btnMonthSubscribe: UIButton! {
        didSet {
            btnMonthSubscribe.isEnabled = false
            btnMonthSubscribe.layer.cornerRadius = 5
            btnMonthSubscribe.addDropShadow(withOffset: CGSize.zero, radius: 5)
            btnMonthSubscribe.setTitle("", for: .selected)
        }
    }
    
    @IBOutlet weak var btnYearSubscription: UIButton! {
        didSet {
            btnYearSubscription.isEnabled = false
            btnYearSubscription.layer.cornerRadius = 5
            btnYearSubscription.addDropShadow(withOffset: CGSize.zero, radius: 5)
            btnYearSubscription.setTitle("", for: .selected)
        }
    }
    @IBOutlet weak var monthActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var yearActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.addDropShadow()
        }
    }
    @IBOutlet weak var lblTagLine: UILabel! {
        didSet {
            lblTagLine.text = "RECORD HOLD'EM HANDS\nREVIEW AT YOUR CONVENIENCE"
        }
    }
    
    @IBOutlet weak var lblOffer: UILabel! {
        didSet {
            lblOffer.text = "YOU CAN ONLY RECORD\n10 GAMES FOR FREE!"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        monthActivityIndicator.startAnimating()
        yearActivityIndicator.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        retriveProductInfo()
    }

    @IBAction func purchaseMonthSubscription(_ sender: UIButton) {
        self.monthActivityIndicator.startAnimating()
        sender.isSelected = true
        self.purchaseSubscription(productId: monthSubscriptionId)
    }
    
    @IBAction func purchaseYearSubscription(_ sender: UIButton) {
        self.yearActivityIndicator.startAnimating()
        sender.isSelected = true
        self.purchaseSubscription(productId: yearSubscriptionId)
    }
    
    @IBAction func restorePurchase(_ sender: UIButton) {
        SVProgressHUD.show()
        SwiftyStoreKit.restorePurchases { (result) in
            let restoredPurchases = result.restoredPurchases
            if let purchase = restoredPurchases.first {
                UserDefaults.standard.set(true, forKey: purchase.productId)
                self.verifyReceipt(forId: purchase.productId)
            } else {
                SVProgressHUD.dismiss()
                UserDefaults.standard.set(false, forKey: monthSubscriptionId)
                UserDefaults.standard.set(false, forKey: yearSubscriptionId)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    AppNotification.showWarningMessage("No purchases to Restore")
                }
            }
        }
    }
    
    func verifyReceipt(forId productId: String) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            self.monthActivityIndicator.stopAnimating()
            self.yearActivityIndicator.stopAnimating()
            SVProgressHUD.dismiss()
            if case .success(let receipt) = result {
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable,
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let expiryDate, let receiptItems):
                    print("Product is valid until \(expiryDate)")
                    UserDefaults.standard.set(true, forKey: productId)
                    SubscriptionExpiryDate = expiryDate
                    self.dismiss(animated: true, completion: nil)
                case .expired(let expiryDate, let receiptItems):
                    AppNotification.showInfoMessage("Product is expired since \(expiryDate.toString())")
                case .notPurchased:
                    print("This product has never been purchased")
                }
            } else {
                // receipt verification error
            }
        }
    }
    
    func purchaseSubscription(productId: String) {
        SwiftyStoreKit.purchaseProduct(productId) { (result) in
            switch result {
            case .success(let purchase):
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                self.verifyReceipt(forId: productId)
            case .error(let error):
                print(error)
            }
        }
    }
    
    func retriveProductInfo() {
        if AppDelegate.shared.reachability.connection == .unavailable {
            self.showAlertMessage("No internet connection", actionName: "Retry") {
                self.retriveProductInfo()
            }
            return
        }
        SwiftyStoreKit.retrieveProductsInfo([monthSubscriptionId, yearSubscriptionId]) { result in
            self.monthActivityIndicator.stopAnimating()
            self.yearActivityIndicator.stopAnimating()
            if result.retrievedProducts.count > 0 {
                if let monthProduct = result.retrievedProducts.first(where: {$0.productIdentifier == monthSubscriptionId}) {
                    let priceString = monthProduct.localizedPrice!
                    self.btnMonthSubscribe.setTitle("Small Blind: " + priceString + " / month", for: UIControl.State.normal)
                    self.btnMonthSubscribe.isEnabled = true
                }
                
                if let yearProduct = result.retrievedProducts.first(where: {$0.productIdentifier == yearSubscriptionId}) {
                    let priceString = yearProduct.localizedPrice!
                    self.btnYearSubscription.setTitle("Big Blind: " + priceString + " / year", for: UIControl.State.normal)
                    self.btnYearSubscription.isEnabled = true
                }
            } else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            } else if let error  = result.error {
                print("Error: \(error)")
                AppNotification.showWarningMessage(error.localizedDescription)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
