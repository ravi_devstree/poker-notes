//
//  NewGameVC.swift
//  Poker Notes
//
//  Created by Ravi on 16/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

class NewGameVC: UIViewController {
    
    var gameType: GameType
    
    init(gameType: GameType) {
        self.gameType = gameType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var btnSave: UIButton! {
        didSet {
            btnSave.cornerRadius = 5
        }
    }
    @IBOutlet weak var btnSaveAndAddNewHand: UIButton! {
        didSet {
            btnSaveAndAddNewHand.cornerRadius = 5
        }
    }
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtBuyIn: UITextField!
    @IBOutlet weak var txtBigBlinds: UITextField!
    @IBOutlet weak var txtSmallBlinds: UITextField!
    @IBOutlet weak var txtTotalPlayers: UITextField!
    
    @IBOutlet var textFieldCollection: [UITextField]!
    
    var onNewGame: ((RealmGame) -> Void)?
    
    var selectedDate = Date() {
        didSet {
            txtDate.text = selectedDate.toString()
        }
    }
    
    lazy var datePicker: UIDatePicker = {
       let picker = UIDatePicker()
        picker.date = selectedDate
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(onDateSelect(_:)), for: UIControl.Event.valueChanged)
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "New Game"
        
        switch gameType {
        case .cash:
            txtBuyIn.isHidden = true
            txtTotalPlayers.isHidden = true
        case .tournament:
            txtSmallBlinds.isHidden = true
            txtBigBlinds.isHidden = true
        }
        
        selectedDate = Date()
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(closeVC(_:)))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        textFieldCollection.forEach { (txt) in
            txt.delegate = self
        }
    }
    
    @IBAction func saveGame(_ sender: UIButton) {
        guard let name = txtName.text, !name.isEmpty,
            let location = txtLocation.text, !location.isEmpty else {return}
        
        switch gameType {
        case .cash:
            guard let smallBlinds = Int(txtSmallBlinds.text ?? ""), let bigBlinds = Int(txtBigBlinds.text ?? "") else {return}
            let newGame = RealmGameManager.shared.newCashGame(name: name, location: location, smallBlinds: smallBlinds, bigBlinds: bigBlinds, date: selectedDate)
            self.dismiss(animated: true) {
                if sender == self.btnSaveAndAddNewHand {
                    self.onNewGame?(newGame)
                }
            }
        case .tournament:
            guard let buyIn = Int(txtBuyIn.text ?? ""),
                let totalPlayer = Int(txtTotalPlayers.text ?? "") else {return}
            let newGame = RealmGameManager.shared.newTournament(name: name, location: location, buyIn: buyIn, totalPlayers: totalPlayer, date: selectedDate)
            self.dismiss(animated: true) {
                if sender == self.btnSaveAndAddNewHand {
                    self.onNewGame?(newGame)
                }
            }
        }
    }
    
    @objc func closeVC(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onDateSelect(_ picker: UIDatePicker) {
        selectedDate = picker.date
    }
}

extension NewGameVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            textField.inputView = datePicker
        }
        return true
    }
}
