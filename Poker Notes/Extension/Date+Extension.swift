//
//  Date+extension.swift
//  Immerch New
//
//  Created by Ravi on 19/09/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import Foundation

extension Date {
    func toString(formate: String? = nil) -> String {
        let formatter = DateFormatter()
        if let formate = formate {
            formatter.dateFormat = formate
        } else {
            formatter.dateStyle = .medium
        }
        return formatter.string(from: self)
    }
    
    func getUTCFormateDate() -> String? {
        let dateFormatter = DateFormatter()
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone = timeZone as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        var dateString: String? = nil
        dateString = dateFormatter.string(from: self)
        dateString = dateString?.replacingOccurrences(of: " ", with: "T")
        dateString = (dateString ?? "") + ("+00:00")
        return dateString
    }

}

extension Int {
    func seconds2Timestamp() -> String {
        let mins:Int = self/60
        let hours:Int = mins/60
        let secs:Int = self%60
        
        if hours == 0 {
            let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
            return strTimestamp
        } else {
            let strTimestamp:String = ((hours<10) ? "0" : "") + String(hours) + ":" + ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
            return strTimestamp
        }
    }
}
