//
//  UIViewController+Extension.swift
//  Immerch New
//
//  Created by Ravi on 19/09/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

extension UIViewController{
    func addBackgroundImage() {
        let bg = UIImageView(image: #imageLiteral(resourceName: "BG"))
        bg.contentMode = .scaleAspectFill
        self.view.addSubview(bg)
        bg.pinEdgesToSuperView()
        self.view.sendSubviewToBack(bg)
        
        if let collectionVC = self as? UICollectionViewController {
            collectionVC.collectionView.backgroundColor = UIColor.black.withAlphaComponent(0.45)
        } else {
            let blackView = UIView()
            blackView.backgroundColor = UIColor.black.withAlphaComponent(0.45)
            bg.addSubview(blackView)
            blackView.pinEdgesToSuperView()
        }
    }
    
    func showAlertMessage(title:String? = nil, _ message:String, actionName: String = "Okay",complition: (() ->())? = nil) {
        var alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        if let title = title {
            alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        }
        alert.addAction(UIAlertAction(title: actionName, style: .default, handler: { (action) in
            if let complition = complition {
                complition()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func popVC(_ sender: Any? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dismissVC(_ sender: Any? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
}
