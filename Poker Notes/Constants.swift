//
//  Constants.swift
//  Poker Notes
//
//  Created by Ravi on 16/12/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import UIKit

enum GameType {
    case cash
    case tournament
}

extension GameType {
    var icon: UIImage? {
        switch self {
        case .cash:
            return #imageLiteral(resourceName: "Cash Game Logo")
        case .tournament:
            return #imageLiteral(resourceName: "Tournament Logo")
        }
    }
}

func DPrint(_ items: Any...) {
    print(items)
}

var selectedCards = [String]() {
    didSet {
        print(selectedCards)
    }
}

var SubscriptionExpiryDate: Date {
    get {
        let timeInterval = UserDefaults.standard.double(forKey: "expiryDate")
        return Date(timeIntervalSince1970: timeInterval)
    }
    set {
        UserDefaults.standard.set(newValue.timeIntervalSince1970, forKey: "expiryDate")
    }
}

let cardRankList = ["A","2","3","4","5","6","7","8","9","10","J","Q","K"]
let cardSuiteList = ["♠️", "♥️", "♦️", "♣️"]
let positionList = ["UTG", "UTG+1", "UTG+2", "MP", "HJ", "CO", "BTN", "SB", "BB"]

let monthSubscriptionId = "com.holdem.sketches.month.subscription"
let yearSubscriptionId = "com.holdem.sketches.year.subscription"
let sharedSecret = "c5bf05b65f92462aa93099e75e958802"

let feedbackEmail: String = "tyrone.tracy@gmail.com"
