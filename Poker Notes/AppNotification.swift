//
//  AppNotification.swift
//  Immerch New
//
//  Created by Ravi on 20/09/19.
//  Copyright © 2019 DevsTree. All rights reserved.
//

import Foundation
import NotificationBannerSwift

class AppNotification {
    
    static var isDisplaying: Bool {
        get {
            return currentBanner?.isDisplaying ?? false
        }
    }
    
    static weak var currentBanner: NotificationBanner?
    
    static func showWarningMessage(title: String? = nil, _ message: String) {
        let banner = NotificationBanner(title: title, subtitle: message, style: .warning)
        currentBanner?.dismiss()
        currentBanner = banner
        banner.show()
    }
    
    static func showSucessMessage(title: String? = nil, _ message: String) {
        let banner = NotificationBanner(title: title, subtitle: message, style: .success)
        currentBanner?.dismiss()
        currentBanner = banner
        banner.show()
    }
    
    static func showInfoMessage(title: String? = nil, _ message: String) {
        let banner = NotificationBanner(title: title, subtitle: message, style: .info)
        currentBanner?.dismiss()
        currentBanner = banner
        banner.show()
    }
    
    static func showErrorMessage(title: String? = nil, _ message: String) {
        let banner = NotificationBanner(title: title, subtitle: message, style: .danger)
        currentBanner?.dismiss()
        currentBanner = banner
        banner.show()
    }
}
